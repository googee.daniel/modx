# Modeled on WordPress
FROM php:7.3-fpm

# Install required extensions
RUN set -ex; \
    \
    savedAptMark="$(apt-mark showmanual)"; \
    \
    apt-get --quiet update; \
    apt-get --quiet --yes --no-install-recommends install \
        libjpeg-dev \
        libmagickwand-dev \
        libpng-dev \
        libzip-dev \
    ; \
    \
    docker-php-ext-configure gd --with-png-dir=/usr --with-jpeg-dir=/usr; \
	docker-php-ext-install -j "$(nproc)" \
		gd \
        mysqli \
		opcache \
        pdo_mysql \
		zip \
	; \
    \
    pecl install --onlyreqdeps imagick; \
    docker-php-ext-enable imagick; \
    \
    pecl install --onlyreqdeps igbinary; \
    docker-php-ext-enable igbinary; \
    \
    pecl install --onlyreqdeps --nobuild redis; \
    cd "$(pecl config-get temp_dir)/redis"; \
    phpize; \
    ./configure --enable-redis --enable-redis-igbinary --enable-redis-lzf; \
    make && make install; \
    docker-php-ext-enable redis; \
    \
    apt-mark auto '.*' > /dev/null; \
    apt-mark manual $savedAptMark; \
    ldd "$(php -r 'echo ini_get("extension_dir");')"/*.so \
        | awk '/=>/ { print $3 }' \
        | sort -u \
        | xargs -r dpkg-query -S \
        | cut -d: -f1 \
        | sort -u \
        | xargs -rt apt-mark manual; \
    \
    apt-get --quiet --yes --auto-remove purge -o APT::AutoRemove::RecommendsImportant=false; \
    apt-get --quiet --yes --no-install-recommends install \
        unzip \
        zip \
    ; \
    rm -rf /var/lib/apt/lists/*; \
    rm -rf /tmp/pear;

# Configure opcache
RUN { \
        echo 'opcache.memory_consumption=128'; \
        echo 'opcache.interned_strings_buffer=8'; \
        echo 'opcache.max_accelerated_files=4000'; \
        echo 'opcache.revalidate_freq=2'; \
        echo 'opcache.fast_shutdown=1'; \
    } > /usr/local/etc/php/conf.d/opcache-recommended.ini

# Configure error logging
RUN { \
        echo 'error_reporting = 4339'; \
        echo 'display_errors = Off'; \
        echo 'display_startup_errors = Off'; \
        echo 'log_errors = On'; \
        echo 'error_log = /dev/stderr'; \
        echo 'log_errors_max_len = 1024'; \
        echo 'ignore_repeated_errors = On'; \
        echo 'ignore_repeated_source = Off'; \
        echo 'html_errors = Off'; \
    } > /usr/local/etc/php/conf.d/error-logging.ini

ENV MODX_VERSION 2.7.1-pl
ENV MODX_SHA1 1fecd1e3cba05eeb9969d47c27ae9bba3ff0011a

RUN set -ex; \
    curl --show-error --fail --location --output modx.zip "https://modx.com/download/direct/modx-${MODX_VERSION}.zip"; \
    echo "$MODX_SHA1 *modx.zip" | sha1sum -c -; \
    unzip modx.zip -d /usr/src; \
    mv /usr/src/modx-${MODX_VERSION} /usr/src/modx; \
    rm modx.zip; \
    chown --recursive www-data:www-data /usr/src/modx;

COPY docker-entrypoint.sh /usr/local/bin/

VOLUME [ "/var/www/html" ]
ENTRYPOINT [ "docker-entrypoint.sh" ]
CMD [ "php-fpm" ]
