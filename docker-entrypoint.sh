#!/bin/bash
set -euo pipefail

if [ "$1" == php-fpm ]; then
    user=www-data
    group=www-data

    if [ ! -e index.php ] && [ ! -e core/docs/version.inc.php ]; then
        if [ "$(id -u)" = '0' ] && [ "$(stat -c '%u:%g' .)" = '0:0' ]; then
            chown "$user:$group" .
        fi

        echo >&2 "MODX not found in $PWD - copying now..."
        sourceTarArgs=(
            --create
            --file -
            --directory /usr/src/modx
            --owner "$user" --group "$group"
        )
        targetTarArgs=(
            --extract
            --file -
        )
        if [ "$user" != '0' ]; then
            # avoid "tar: .: Cannot utime: Operation not permitted" and "tar: .: Cannot change mode to rwxr-xr-x: Operation not permitted"
            targetTarArgs+=( --no-overwrite-dir )
        fi
        tar "${sourceTarArgs[@]}" . | tar "${targetTarArgs[@]}"
        echo >&2 "Complete! MODX has been successfully copied to $PWD"
    fi

    envs=(
        MODX_DB_HOST
        MODX_DB_USER
        MODX_DB_PASSWORD
        MODX_DB_NAME
        MODX_DB_CHARSET
        MODX_DB_COLLATE
        MODX_TABLE_PREFIX
        MODX_ADMIN_USER
        MODX_ADMIN_PASSWORD
        MODX_ADMIN_EMAIL
    )

    haveConfig=
    for e in "${envs[@]}"; do
        if [ -z "$haveConfig" ] && [ -n "${!e}" ]; then
            haveConfig=1
        fi
    done

    if [ "$haveConfig" ]; then
        : "${MODX_DB_HOST:=mysql}"
        : "${MODX_DB_USER:=root}"
        : "${MODX_DB_PASSWORD:=}"
        : "${MODX_DB_NAME:=modx}"
        : "${MODX_DB_CHARSET:=utf8}"
        : "${MODX_DB_COLLATE:=utf8_general_ci}"
        : "${MODX_TABLE_PREFIX:=modx_}"
        : "${MODX_ADMIN_USER:=admin}"
        : "${MODX_ADMIN_PASSWORD:=}"
        : "${MODX_ADMIN_EMAIL:='admin@example.com'}"

        if [ ! -e core/config/config.inc.php ]; then
            # Initial install
            TERM=dumb php -- <<'EOPHP'
<?php
$config = array(
    'database_type' => 'mysql',
    'database_server' => getenv('MODX_DB_HOST'),
    'database' => getenv('MODX_DB_NAME'),
    'database_user' => getenv('MODX_DB_USER'),
    'database_password' => getenv('MODX_DB_PASSWORD'),
    'database_connection_charset' => 'utf8',
    'database_charset' => 'utf8',
    'database_collation' => 'utf8_general_ci',
    'table_prefix' => 'modx_',
    'https_port' => 443,
    'http_host' => 'localhost',
    'cache_disabled' => 0,
    'inplace' => 1,
    'unpacked' => 0,
    'language' => 'en',
    'cmsadmin' => getenv('MODX_ADMIN_USER'),
    'cmspassword' => getenv('MODX_ADMIN_PASSWORD'),
    'cmsadminemail' => getenv('MODX_ADMIN_EMAIL'),
    'core_path' => '/var/www/html/core/',
    'context_mgr_path' => '/var/www/html/manager/',
    'context_mgr_url' => '/manager/',
    'context_connectors_path' => '/var/www/html/connectors/',
    'context_connectors_url' => '/connectors/',
    'context_web_path' => '/var/www/html/',
    'context_web_url' => '/',
    'remove_setup_directory' => true
);
$xml = new \DOMDocument('1.0', 'utf-8');
$modx = $xml->createElement('modx');
foreach ($config as $key => $value) {
    $modx->appendChild($xml->createElement($key, htmlentities($value, ENT_QUOTES|ENT_XML1)));
}
$xml->appendChild($modx);
$fh = fopen('/var/www/html/setup/config.xml', "w");
echo $xml->saveXML();
fwrite($fh, $xml->saveXML());
fclose($fh);
EOPHP

            php setup/index.php --installmode=new --config=/var/www/html/setup/config.xml
        fi
    fi

    for e in "${envs[@]}"; do
        unset "$e"
    done
fi

exec "$@"
